import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import QRect
from QtUiHelper import *
from LogViewHelper import LogViewer
from StockFinder import StockFinder
from TradeAPIs import Kiwoom
from TradeAPIs import WINDOW
from PyQt5.QtWidgets import *

class TradeWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Py AutoTrader, TED (2021)")
        self.setGeometry(0,0,600, 170)
        self.logger = LogViewer()
        self.finder = StockFinder()

        # After api install
        self.api = Kiwoom.shared(self)
        self.api.addHandler(self.apiRequstDone)

        self.loginBtn = UiHelper.createButton(self, "Login", self.loginClickHandler)
        self.stateTbox = QTextEdit(self)
        self.stateTbox.setReadOnly(True)
        self.stateTbox.setMaximumHeight(30)
        self.searchBtn = UiHelper.createButton(self, "AutoRun", self.startAutoHandler)
        self.accountLabel = QLabel("Accound number", self)
        self.idLabel = QLabel("ID", self)
        self.nameLabel = QLabel("Name", self)
        self.accountTbox = QTextEdit(self)
        self.accountTbox.setReadOnly(True)
        self.accountTbox.setMaximumHeight(30)
        self.idTbox = QTextEdit(self)
        self.idTbox.setReadOnly(True)
        self.idTbox.setMaximumHeight(30)
        self.nameTbox = QTextEdit(self)
        self.nameTbox.setReadOnly(True)
        self.nameTbox.setMaximumHeight(30)
        self.searchBtn.setEnabled(False)
        self.stateTbox.setText("OFFLINE")

        # setLayout
        mainLayout = QGridLayout()
        col = 0
        raw = 0
        mainLayout.addWidget(self.loginBtn, col, raw, 1,5)
        raw+=5
        mainLayout.addWidget(self.stateTbox, col, raw)
        col+=1
        raw=0
        mainLayout.addWidget(self.accountLabel, col, raw)
        raw+=1
        mainLayout.addWidget(self.accountTbox, col, raw)
        raw+=1
        mainLayout.addWidget(self.idLabel, col, raw)
        raw+=1
        mainLayout.addWidget(self.idTbox, col, raw)
        raw+=1
        mainLayout.addWidget(self.nameLabel, col, raw)
        raw+=1
        mainLayout.addWidget(self.nameTbox, col, raw)
        col+=1
        mainLayout.addWidget(self.searchBtn, col, 0, 1,6)
        # merge layout
        self.setLayout(mainLayout)

        # event handler bind
        self.eventBinder()

        # Api
    def apiRequstDone(self, resultTupple):
        trcode = resultTupple[2]
        self.logger.log(str(trcode) + " api request done")

    def eventBinder(self):
        self.api.eventLoginConnecter(self.loginHandler)

    def loginClickHandler(self):
        self.logger.log("login button")
        self.api.cmdCall("CommConnect()")
    
    def loginHandler(self, res):
        if res == 0:
            self.logger.log("Login Success")
            self.logger.showLogDialog()
            self.finder.showLogDialog()
            self.getAccountInfomation()
            self.searchBtn.setEnabled(True)
            self.stateTbox.setText("ONLINE")
        else :
            self.logger.log("Login Failed reason : %d" % res)
    
    def startAutoHandler(self):
        self.logger.log("Stock Finder Start ...")
        self.finder.autoTradeStart()
        # self.finder.getTradeTradeVolume()
    
    def getAccountInfomation(self):
        try:
            info = self.api.getAccountInfomation()
            self.accountTbox.setText(info[0])
            self.idTbox.setText(info[1])
            self.nameTbox.setText(info[2])

        except Exception as err:
            print(str(err))

if __name__ == "__main__":
    app = QApplication(sys.argv)
    trade = TradeWindow()
    trade.show()
    app.exec_()
