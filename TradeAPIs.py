import sys
import time
from PyQt5.QAxContainer import *
from PyQt5.QtCore import *
WINDOW = True
# WINDOW = False

class Kiwoom():
    sharedInstance = None
    def __init__(self):
        # self.kiwoom = None
        self.kiwoom = QAxWidget("KHOPENAPI.KHOpenAPICtrl.1")
        self.handler = []
        self.tr_event_loop = None
    
    def shared(self):
        if Kiwoom.sharedInstance == None:
            Kiwoom.sharedInstance = Kiwoom()
        return Kiwoom.sharedInstance

    def addHandler(self, handler):
        self.handler.append(handler)
    
    def setValues(self, valueDict):
        if WINDOW:
            for k in valueDict:
                v = valueDict[k]
                print(k + ", " + valueDict[k])
                self.kiwoom.dynamicCall("SetInputValue(QString, QString)", k, v)

    def requestRun(self, trCode, displayNum, option):
        if WINDOW:
            time.sleep(0.22)
            print("request run " + trCode)
            self.kiwoom.dynamicCall("CommRqData(QString, QString, int, QString)"
                                    ,trCode+"Req", trCode, displayNum, option)
    
    def trHandler(self, screen_no, rqname, trcode, recordname, prev_next, data_len, err_code, msg1, msg2):
        print("request run Done, " + trcode)
        pack = (screen_no, rqname, trcode, recordname, prev_next, data_len, err_code, msg1, msg2)
        if self.handler != None : 
            for h in self.handler:
                try:
                    h(pack)
                except Exception as e :
                    print("ERROR : " + str(e))

    def getData(self, trcode, recordname, option, index=0):
        if WINDOW:
            return self.kiwoom.dynamicCall("GetCommData(QString, QString, int, QString"
                                            , trcode, recordname, index, option)
    
    def getDataLength(self, trcode, rqname):
        ret = self.kiwoom.dynamicCall("GetRepeatCnt(QString, QString)", trcode, rqname)
        return ret

    def cmdCall(self, cmd):
        if WINDOW:
            self.kiwoom.dynamicCall(cmd)
    
    def eventLoginConnecter(self, handler):
        if WINDOW:
            self.kiwoom.OnEventConnect.connect(handler)
            self.kiwoom.OnReceiveTrData.connect(self.trHandler)

    def getAccountInfomation(self):
        if WINDOW:
            accountInfo = self.kiwoom.dynamicCall("GetLoginInfo(QString)", ["ACCNO"])
            userId = self.kiwoom.dynamicCall("GetLoginInfo(QString)", ["USER_ID"])
            userName = self.kiwoom.dynamicCall("GetLoginInfo(QString)", ["USER_NAME"])
            return (accountInfo, userId, userName)


    