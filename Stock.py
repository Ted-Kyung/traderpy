from TradeAPIs import Kiwoom
from pandas import DataFrame
# 트랜잭션 리퀘스트 네임에 자기 코드를 넣도록 하자 
# 외국계 기본 정보에 있는 "외국계 사용 "
# 장중 확인필요 
class MStock():
    def __init__(self, code):
        self._code = code
        self._trace = True
        print("Build Stock code :" + str(self._code))
        self.kw = Kiwoom.shared(self)
        self.kw.addHandler(self.apiRequstDone)
        self.info = {}
        self.df = DataFrame(range[:99], columns=['index'])

        getForigenVolumes(self)
        getCompanyVolumes(self)
        getPrices(self)
    
    def getForigenVolumes(self):
        print("call getForigenVolumes :" + str(self._code))

    def getCompanyVolumes(self):
        print("call getCompanyVolumes :" + str(self._code))

    def getPrices(self):
        print("call getPrices :" + str(self._code))
    
    def getName(self):
        print("call getName")
    
    def getCode(self):
        print("call getCode")
    
    def ignore(self):
        self._trace = False
    
    def isTraced(self):
        return self._trace
    
    def update(self):
        if self._trace:
            print("call update : " + self.getName())
        else:
            print("update ignore stock passed : " + self.getName())
    
    def createUpdateTime(self):
        print("call createUpdateTime : " + self.getName())

    def stopUpdateTime(self):
        print("call stopUpdateTime : " + self.getName())

    def showPlot(self):
        print("show " + self.getName() + " plot")

    def apiRequstDone(self, resultTupple):
        trcode = resultTupple[2]
        print("in MStock] api handler : " + trcode)