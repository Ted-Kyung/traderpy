import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

class UiHelper():
    def __init__(self):
        super.__init__(self)
    
    def createButton(self, label, handler):
        button = QPushButton(label, self)
        button.clicked.connect(handler)
        return button
    