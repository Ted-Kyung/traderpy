import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import datetime 
import time 
from QtUiHelper import *
import inspect
from TradeAPIs import Kiwoom
from Stock import MStock
from matplotlib import pyplot as plt
from pandas import DataFrame

class StockFinder(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.list = RequiredResearchList.shared(self)

        # set Dialog Instance
        self.dialog = QDialog()
        self.dialog.setGeometry(0,500,800, 500)

        # After api install
        self.kw = Kiwoom.shared(self)
        self.kw.addHandler(self.apiRequstDone)

        # set TestEdit 
        self.dialog.setWindowTitle("Auto Search")
        self.textEdit = QTextEdit(self.dialog)
        self.textEdit.setEnabled(True)
        self.textEdit.verticalScrollBar().setValue(self.textEdit.verticalScrollBar().minimum())

        # set Layout
        self.mainLayout = QVBoxLayout()
        layOut = QVBoxLayout()
        layOut.addWidget(self.textEdit)
        self.mainLayout.addLayout(layOut)
        self.dialog.setLayout(self.mainLayout)
        self.calculationIndex = 0
        self.daysPrice = []
        self.daysTradeVolume = []
        self.stock = {} # code, name, per, ... + df


    def showLogDialog(self):
        print("show finder dialog")
        self.dialog.show()
    
    def log(self, log):
        if len(log) <= 0: 
            return
        currentTime = datetime.datetime.now()
        timeStr = str(currentTime).split(".")[0]
        self.textEdit.append(timeStr + " : " + log)

    # Api
    # pack = (screen_no, rqname, trcode, recordname, prev_next, data_len, err_code, msg1, msg2)
    def apiRequstDone(self, resultTupple):
        trcode = resultTupple[2]
        print("api request done : " + trcode)
        if trcode  == "opt10036":
            self.handleStocksBulk(resultTupple)
        elif trcode == "opt10034": # 외인 기간별"순매수" + # "opt10044" # 기관 당일 "순매수" 
            self.handleStocksBulk(resultTupple)
        #     self.getStockInfomation(self.calculationIndex) # 데이터 1 종목씩 검사 시작 
        # elif trcode  == "opt10001": # 한 종목 주식 정보 불러오기
        #     self.stock = {} # init
        #     result = self.handleStockDetail(resultTupple)
        #     if result == 0: # 검사를 통과함, 주가 불러오기
        #         self.daysPrice = []
        #         self.getStockDaysPrice(self.stock['code'])
        #     elif result == 1: # 검사를 통과 못함 drop, 다음 주식 조사하기
        #         self.nextResearch()
        # elif trcode  == "opt10081": # 한종목 주식의 일별 주가 불러오기 
        #     action = self.handleStockDaysPrice(resultTupple) # action : 1 : 매수 가능, 0 : 무시 
        #     self.nextResearch()
        else:
            print("Unknown transaction : " + trcode)
    
    def autoTradeStart(self):
        self.calculationIndex = 0
        # self.getTradeTop100() # 거래량 상위 100개
        self.getForiginTradeTops() # 외인 순매수 상위 100개 

    def nextResearch(self):
        self.calculationIndex+=1
        if len(self.list.getCodesKey()) > self.calculationIndex: # 데이터 풀 끝이 아니면, 다음 
            self.getStockInfomation(self.calculationIndex)
        # else: # 데이터 풀의 마지막 이면, 리로드.
            # self.reloadAll()

    # def reloadAll(self):
    #     self.calculationIndex = 0
    #     self.list.removeAll() # 모든 데이터 삭제
    #     self.autoTradeStart() # 재검색 

    def getTradeTop100(self):
        v = {}
        v["시장구분"]="000"
        v["기간"]="0"
        self.log("get Trade Top 100")
        self.kw.setValues(v)
        self.kw.requestRun("opt10036", 0, "화면번호")

    def getForiginTradeTops(self):
        v = {}
        v["시장구분"]="101"
        v["매매구분"]="2"
        v["기간"]="0"
        self.log("get Forigin Trade Tops");
        self.kw.setValues(v)
        self.kw.requestRun("opt10034", 0, "화면번호")

    def getStockInfomation(self, index=0):
        stockCodeDict = self.list.getCodeWithName()
        code = self.list.getCodesKey()[index]
        print(stockCodeDict[code] +"] " + code + " researching..")
        # opt10001 주식 정보 (PER / 순익 등 ..)
        v = {}
        v["종목코드"]=code
        print("get stock infomation : " + code)
        self.kw.setValues(v)
        self.kw.requestRun("opt10001", 0, "화면번호")
    
    def getStockDaysPrice(self, code):
        v = {}
        v["종목코드"]=code
        print("get stock Days : " + code)
        self.kw.setValues(v)
        self.kw.requestRun("opt10081", 0, "화면번호")
    
    def handleStockDaysPrice(self, resultTupple):
        trCode = resultTupple[2]
        recordName = resultTupple[3]
        isContinue = resultTupple[4]
        if len(self.daysPrice) == 0:
            self.daysPrice = []
            self.daysTradeVolume = []

        for i in range(self.kw.getDataLength(trCode, recordName)):
            try:
                self.daysPrice.append(self.kw.getData(trCode, recordName, "현재가", index=i).strip())
                self.daysTradeVolume.append(self.kw.getData(trCode, recordName, "거래량", index=i).strip())
            except Exception as e:
                print(e)
                break

        self.daysPrice = [int (i) for i in self.daysPrice]
        self.daysTradeVolume= [int (i) for i in self.daysTradeVolume]
        reversePrice = list(self.daysPrice[:365]).copy()
        reverseTradeVolume = list(self.daysTradeVolume[:365]).copy()
        list.reverse(reversePrice) # index 0 = oldest
        list.reverse(reverseTradeVolume) # index 0 = oldest
        s=12
        l=26
        signal=9
        df = DataFrame(reversePrice, columns=['price'])
        df['tVolume'] = reverseTradeVolume
        df['MACD'] =  df['price'].ewm( span = s, min_periods = s-1 , adjust=False).mean() - df['price'].ewm( span = l, min_periods = l-1 , adjust=False).mean()
        df['MACD_Signal'] = df['MACD'].ewm( span = signal, min_periods = signal-1, adjust=False).mean()
        df['MACD_OSC'] = df['MACD'] - df['MACD_Signal']

        self.stock['df'] = df

        # plot
        dFrame = self.stock['df']
        fig = plt.figure()
        fig.suptitle(self.stock['code'])
        p1 = fig.add_subplot(2, 1, 1)
        p2 = fig.add_subplot(2, 1, 2)
        p11 = p1.twinx()
        p22 = p2.twinx()
        p1.plot(range(len(dFrame['price'][-150:])),dFrame['price'][-150:], color='black')
        p11.bar(range(len(dFrame['MACD_OSC'][-150:])),dFrame['MACD_OSC'][-150:], color=['r' if v >= 0 else 'b' for v in dFrame["MACD_OSC"][-150:]])
        p2.plot(range(len(dFrame['price'][-45:])),dFrame['price'][-45:], color='black')
        p22.bar(range(len(dFrame['MACD_OSC'][-45:])),dFrame['MACD_OSC'][-45:], color=['r' if v >= 0 else 'b' for v in dFrame['MACD_OSC'][-45:]])
        plt.pause(1)
        plt.close()
        # ...
        # 가격, MACD, 거래량 분석 "매수가능여부확인"
        # ...
        return 1


    def handleStockDetail(self, resultTupple):
        trCode = resultTupple[2]
        recordName = resultTupple[3]
        self.stock['name'] = self.kw.getData(trCode, recordName, "종목명").strip()
        self.stock['code'] = self.kw.getData(trCode, recordName, "종목코드").strip()
        self.stock['cost'] = self.kw.getData(trCode, recordName, "현재가").strip()
        self.stock['PER'] = self.kw.getData(trCode, recordName, "PER").strip()
        self.stock['EPS'] = self.kw.getData(trCode, recordName, "EPS").strip()
        self.stock['PBR'] = self.kw.getData(trCode, recordName, "PBR").strip()
        self.stock['ROE'] = self.kw.getData(trCode, recordName, "ROE").strip()
        self.stock['BPS'] = self.kw.getData(trCode, recordName, "BPS").strip()

        result = -1
        checkCompanyInfomation = 0
        #PER / ROC ... 검사 
        self.log(self.stock['name'] + " 재무표검사, PER : " + self.stock['PER'] +", EPS : " + self.stock['EPS'] +", PBR : " + self.stock['PBR'] + ", ROE : " + self.stock['ROE'])
        print(self.stock['name'] + " 재무표검사, PER : " + self.stock['PER'] +", EPS : " + self.stock['EPS'] +", PBR : " + self.stock['PBR'] + ", ROE : " + self.stock['ROE'])
        # ... 
        # 재무표검사필터구현필요 
        # ...
        def passStock():
            return 0 # pass

        # 재무재표검사
        if len(self.stock['EPS']) == 0:
            self.log(self.stock['name'] + "] 재무검사 EPS X")
            return 0 # pass
        else:
            eps = float(self.stock["EPS"])
            if eps < 0:
                self.log(self.stock['name'] + "] 재무검사 EPS 세후순이익/주식수 < 0")
                return 0 # pass 

        if len(self.stock['PER']) == 0:
            self.log(self.stock['name'] + "] 재무검사 PER X")
            return 0 # pass
        else:
            per = float(self.stock["PER"])
            if per < 0 and per > 30:
                self.log(self.stock['name'] + "] 재무검사 PER, 투자금회수기간 " + str(per) + "년, Pass")
                return 0
        
        if len(self.stock['BPS']) == 0 :
            self.log(self.stock['name'] + "] 재무검사 BPS X")
            return 0 # pass
        else:
            bpsGap = int(self.stock['BPS']) - int(self.stock['cost'])
            if bpsGap < 0 and bpsGap < float(self.stock['cost']) * -0.33:
                self.log(self.stock['name'] + "] 현재가 - 주당순자산가치 미달 : " + str(bpsGap))
                return 0 # pass
        
        if len(self.stock['PBR']) == 0 :
            self.log(self.stock['name'] + "] 재무검사 PBR X")
            return 0 # pass
        else:
            pbr = float(self.stock['PBR'])
            if pbr > 1.5:
                self.log(self.stock['name'] + "] 재무검사 PBR 1.5 < " + str(pbr))
                return 0 # pass
        
        if len(self.stock['ROE']) == 0 :
            self.log(self.stock['name'] + "] 재무검사 ROE X")
            return 0 # pass
        else:
            roe = float(self.stock['ROE'])
            if roe < 5.5:
                self.log(self.stock['name'] + "] 재무검사 ROE 투자대비수익률(%)미달 : " + str(roe))
                return 0 # pass

        if checkCompanyInfomation == 0:
            result = 0 # paass
        else:
            result = 1 # not passed, remove from List
        return result

    def handleStocksBulk(self, resultTupple):
        trCode = resultTupple[2]
        recordName = resultTupple[3]
        for i in range(self.kw.getDataLength(trCode, recordName)):
            n = self.kw.getData(trCode, recordName, "종목명", index=i)
            c = self.kw.getData(trCode, recordName, "거래량", index=i)
            cd = self.kw.getData(trCode, recordName, "종목코드", index=i)
            p = self.kw.getData(trCode, recordName, "현재가", index=i)
            n = n.strip()
            c = c.strip()
            cd = cd.strip()
            p = str(p.strip()).replace("+-", "")
            print("" + str(i) + "]name:" +n + ", pradeVolume:" + c + ", price:" + p)
            s = MStock(cd)
            # RequiredResearchList.shared(self).addCodeWithName(cd, s)
            self.log("\n Build " + str(n) +":[" + str(cd) + "] Stock\n")
            time.sleep(0.15)

class RequiredResearchList:
    sharedInstance = None
    def __init__(self):
        self.stockCodeList = {}
        self.stockDataFrameList = {}
        self.tradeCode = {}

    def shared(self):
        if RequiredResearchList.sharedInstance == None:
            RequiredResearchList.sharedInstance = RequiredResearchList()
        return RequiredResearchList.sharedInstance

    def addCodeWithName(self, code, name):
        self.stockCodeList[code] = name

    def addCodeWithDataFrame(self, code, dataFrame):
        self.stockDataFrameList[code] = dataFrame

    def addTradeDataFrame(self, code, dataFrame):
        self.tradeCode[code] = dataFrame
    
    def getCodesKey(self):
        return list(self.stockCodeList.keys())
    
    def getDataFrameKey(self):
        return list(self.stockDataFrameList.keys())

    def getCodeWithName(self):
        return self.stockCodeList

    def getDataFrame(self, code):
        return self.stockDataFrameList[code]
    
    def getName(self, code):
        return self.stockCodeList[code]
    
    def removeAll(self):
        self.stockCodeList = {}
        self.stockDataFrameList = {}
    
    def remove(self, code):
        del self.stockCodeList[code]
        del self.stockDataFrameList[code]


# if __name__ == "__main__":
#     # for test
#     testValues = ['15850', '16200', '16000', '15950', '15850', '15200', '15300', '14800', '14700', '14450', '14900', '15050', '14450', '14100', '14200', '14750', '14700', '14250', '14050', '14250', '14600', '15050', '16400', '16450', '16200', '15900', '16000', '16000', '16050', '16050', '16250', '15650', '15350', '15100', '14800', '14750', '14750', '15000', '15000', '15050', '14800', '14450', '14700', '15000', '15250', '15500', '15550', '15300', '15050', '15150', '15150', '15250', '14800', '14850', '15100', '14800', '14200', '14100', '14550', '14300', '14650', '14500', '13850', '14500', '14900', '14650', '15150', '15500', '15200', '14600', '14400', '14200', '13600', '13550', '13550', '13700', '13950', '13900', '13350', '13300', '13150', '12850', '12750', '12750', '12750', '12950', '12700', '13050', '13200', '13150', '13150', '13200', '13000', '12800', '12700', '12650', '12700', '12700', '12600', '12750', '12650', '12650', '12750', '12800', '12650', '12700', '12900', '12550', '12700', '12350', '12850', '12850', '13400', '13550', '13450', '12950', '12550', '12400', '12600', '12550', '12550', '12150', '12400', '12600', '12700', '12650', '12700', '12800', '12850', '13200', '12850', '12750', '13000', '12900', '12700', '12600', '12450', '12450', '12750', '12750', '12700', '12950', '12950', '12900', '12650', '12650', '12750', '13050', '13350', '13600', '13550', '13650', '13750', '13900', '14250', '14200', '13350', '14300', '14750', '14800', '14850', '14550', '14800', '15050', '15000', '15000', '14800', '14550', '14700', '15000', '15050', '15050', '15150', '15100', '15050', '14700', '14950', '14800', '14200', '13800', '13600', '13750', '13750', '13600', '13550', '13550', '13850', '13900', '13800', '13600', '13700', '13700', '13900', '13750', '13750', '13700', '13850', '13550', '13600', '12400', '12400', '12300', '12300', '12250', '12500', '12000', '12200', '11600', '11650', '11200', '10300', '9700', '9450', '9900', '9180', '11600', '12000', '12700', '12900', '13300', '13800', '13750', '14250', '14650', '14800', '14800', '14650', '15000', '14700', '14950', '14900', '14750', '14800', '14750', '14750', '14700', '15150', '15200', '15250', '15450', '15650', '15600', '15600', '15850', '15700', '15600', '15450', '15450', '15700', '15450', '15400', '15350', '15850', '16150', '16300', '16400', '16600', '16450', '16450', '16450', '16400', '16500', '16350', '16150', '16400', '16650', '17150', '17150', '17850', '18050', '18600', '18450', '18350', '18450', '18750', '18850', '18550', '18550', '18500', '18650', '18200', '18150', '18350', '18150', '18450', '18450', '18350', '18350', '18400', '18450', '18400', '18050', '18200', '18400', '18750', '18500', '18800', '18650', '18750', '18750', '19200', '19600', '19550', '19200', '19150', '18700', '18950', '18900', '18500', '18500', '18400', '18550', '18350', '18800', '18900', '18700', '18750', '19050', '18750', '19100', '19400', '18950', '19150', '18850', '18300', '18050', '18000', '18200', '18450', '19050', '19100', '19100', '19500', '18800', '18800', '18900', '18700', '18800', '19050', '18800', '19150', '18850', '18950', '18100', '18050', '18000', '18050', '18100', '18150', '17950', '17800', '17800', '17700', '18100', '18450', '19250', '18500', '18250', '18550', '18200', '18350', '18850', '18800', '19000', '19450', '18750', '19250', '19300', '19650', '20000', '19900', '19350', '19450', '19900', '19650', '19650', '19800', '19550', '19650', '19800', '19600', '19500', '19450', '19450', '19450', '19500', '19550', '19700', '19750', '19850', '19500', '19850', '20400', '20700', '20750', '20650', '20600', '21050', '21000', '21050', '21350', '21500', '21450', '21800', '22250', '21750', '21950', '21350', '21000', '22050', '22250', '21900', '21850', '21350', '21500', '21550', '22100', '21750', '22000', '21950', '22200', '22100', '21850', '22150', '22050', '22400', '22650', '23600', '23550', '24100', '23700', '23650', '23800', '23800', '23500', '23400', '23600', '23850', '23450', '23500', '23250', '23750', '23850', '23900', '24100', '23900', '24050', '23650', '23900', '24650', '24450', '24650', '25050', '25600', '25400', '24300', '24200', '24150', '23700', '23050', '23400', '23700', '24150', '23800', '23750', '24500', '23850', '23400', '23250', '22700', '22750', '22600', '22600', '22150', '22400', '22100', '22650', '22600', '23000', '23000', '23100', '23050', '23350', '23050', '23300', '23400', '23250', '23250', '22750', '22700', '22550', '22800', '22200', '22600', '22800', '22050', '22300', '22450', '22000', '22400', '22400', '22400', '22300', '22300', '22600', '22350', '22350', '21850', '22100', '22450', '22000', '21700', '20900', '21350', '21850', '21550', '21950', '21700', '21900', '21800', '21100', '20750', '21250', '21800', '21500', '21600', '21500', '21550', '21550', '21800', '22150', '21200', '21200', '21200', '21650', '21800', '21900', '22100', '22000', '22600', '22000', '21700', '20750', '21300', '20700', '20500', '20650', '20550', '18700', '18000', '17950', '18000', '17900', '17800', '17800', '18200', '17650', '18100', '18600', '18550', '18750', '18400', '19000', '18400', '18450', '18550', '18300', '18400', '18500', '18750', '19250', '19450', '19450', '19450', '19450', '19600', '19350', '19600', '18550', '18350', '18100', '18400', '18350', '18550', '18300', '17800', '17900', '18100', '18250', '18300', '18400', '18400', '18550', '18250', '18000', '17950', '18150', '18150', '18550', '18550']
    


# 한달 주식 일주월일시분 요청 (opt10005) -> 외인 및 기관 매/수매 상황 
# 금일 외인 및 기관 매/수매 상황  (opt10006) -> 외인 및 기관 매/수매 상황 
# 90일 데이터 획득 (opt10015) -> 장중/후 거래량 및 금액 90 일간 변동 파악
# opt10032 거래대금 상위 요청 
# opt10080 분봉
# opt10081 일봉
# 시장 평균가 구해오기 

# 전략 구현 https://bosungs2y.tistory.com/261

# 000060 메리츠화재 
# ['15850', '16200', '16000', '15950', '15850', '15200', '15300', '14800', '14700', '14450', '14900', '15050', '14450', '14100', '14200', '14750', '14700', '14250', '14050', '14250', '14600', '15050', '16400', '16450', '16200', '15900', '16000', '16000', '16050', '16050', '16250', '15650', '15350', '15100', '14800', '14750', '14750', '15000', '15000', '15050', '14800', '14450', '14700', '15000', '15250', '15500', '15550', '15300', '15050', '15150', '15150', '15250', '14800', '14850', '15100', '14800', '14200', '14100', '14550', '14300', '14650', '14500', '13850', '14500', '14900', '14650', '15150', '15500', '15200', '14600', '14400', '14200', '13600', '13550', '13550', '13700', '13950', '13900', '13350', '13300', '13150', '12850', '12750', '12750', '12750', '12950', '12700', '13050', '13200', '13150', '13150', '13200', '13000', '12800', '12700', '12650', '12700', '12700', '12600', '12750', '12650', '12650', '12750', '12800', '12650', '12700', '12900', '12550', '12700', '12350', '12850', '12850', '13400', '13550', '13450', '12950', '12550', '12400', '12600', '12550', '12550', '12150', '12400', '12600', '12700', '12650', '12700', '12800', '12850', '13200', '12850', '12750', '13000', '12900', '12700', '12600', '12450', '12450', '12750', '12750', '12700', '12950', '12950', '12900', '12650', '12650', '12750', '13050', '13350', '13600', '13550', '13650', '13750', '13900', '14250', '14200', '13350', '14300', '14750', '14800', '14850', '14550', '14800', '15050', '15000', '15000', '14800', '14550', '14700', '15000', '15050', '15050', '15150', '15100', '15050', '14700', '14950', '14800', '14200', '13800', '13600', '13750', '13750', '13600', '13550', '13550', '13850', '13900', '13800', '13600', '13700', '13700', '13900', '13750', '13750', '13700', '13850', '13550', '13600', '12400', '12400', '12300', '12300', '12250', '12500', '12000', '12200', '11600', '11650', '11200', '10300', '9700', '9450', '9900', '9180', '11600', '12000', '12700', '12900', '13300', '13800', '13750', '14250', '14650', '14800', '14800', '14650', '15000', '14700', '14950', '14900', '14750', '14800', '14750', '14750', '14700', '15150', '15200', '15250', '15450', '15650', '15600', '15600', '15850', '15700', '15600', '15450', '15450', '15700', '15450', '15400', '15350', '15850', '16150', '16300', '16400', '16600', '16450', '16450', '16450', '16400', '16500', '16350', '16150', '16400', '16650', '17150', '17150', '17850', '18050', '18600', '18450', '18350', '18450', '18750', '18850', '18550', '18550', '18500', '18650', '18200', '18150', '18350', '18150', '18450', '18450', '18350', '18350', '18400', '18450', '18400', '18050', '18200', '18400', '18750', '18500', '18800', '18650', '18750', '18750', '19200', '19600', '19550', '19200', '19150', '18700', '18950', '18900', '18500', '18500', '18400', '18550', '18350', '18800', '18900', '18700', '18750', '19050', '18750', '19100', '19400', '18950', '19150', '18850', '18300', '18050', '18000', '18200', '18450', '19050', '19100', '19100', '19500', '18800', '18800', '18900', '18700', '18800', '19050', '18800', '19150', '18850', '18950', '18100', '18050', '18000', '18050', '18100', '18150', '17950', '17800', '17800', '17700', '18100', '18450', '19250', '18500', '18250', '18550', '18200', '18350', '18850', '18800', '19000', '19450', '18750', '19250', '19300', '19650', '20000', '19900', '19350', '19450', '19900', '19650', '19650', '19800', '19550', '19650', '19800', '19600', '19500', '19450', '19450', '19450', '19500', '19550', '19700', '19750', '19850', '19500', '19850', '20400', '20700', '20750', '20650', '20600', '21050', '21000', '21050', '21350', '21500', '21450', '21800', '22250', '21750', '21950', '21350', '21000', '22050', '22250', '21900', '21850', '21350', '21500', '21550', '22100', '21750', '22000', '21950', '22200', '22100', '21850', '22150', '22050', '22400', '22650', '23600', '23550', '24100', '23700', '23650', '23800', '23800', '23500', '23400', '23600', '23850', '23450', '23500', '23250', '23750', '23850', '23900', '24100', '23900', '24050', '23650', '23900', '24650', '24450', '24650', '25050', '25600', '25400', '24300', '24200', '24150', '23700', '23050', '23400', '23700', '24150', '23800', '23750', '24500', '23850', '23400', '23250', '22700', '22750', '22600', '22600', '22150', '22400', '22100', '22650', '22600', '23000', '23000', '23100', '23050', '23350', '23050', '23300', '23400', '23250', '23250', '22750', '22700', '22550', '22800', '22200', '22600', '22800', '22050', '22300', '22450', '22000', '22400', '22400', '22400', '22300', '22300', '22600', '22350', '22350', '21850', '22100', '22450', '22000', '21700', '20900', '21350', '21850', '21550', '21950', '21700', '21900', '21800', '21100', '20750', '21250', '21800', '21500', '21600', '21500', '21550', '21550', '21800', '22150', '21200', '21200', '21200', '21650', '21800', '21900', '22100', '22000', '22600', '22000', '21700', '20750', '21300', '20700', '20500', '20650', '20550', '18700', '18000', '17950', '18000', '17900', '17800', '17800', '18200', '17650', '18100', '18600', '18550', '18750', '18400', '19000', '18400', '18450', '18550', '18300', '18400', '18500', '18750', '19250', '19450', '19450', '19450', '19450', '19600', '19350', '19600', '18550', '18350', '18100', '18400', '18350', '18550', '18300', '17800', '17900', '18100', '18250', '18300', '18400', '18400', '18550', '18250', '18000', '17950', '18150', '18150', '18550', '18550']
# 현재 -> 과거 

# 시나리오 1
# class StockBuilder 필요 : build(code)
    # setIgnore()
    # set update() -> 분봉 "opt10080"
    # 일별 주가 -> opt10081
    # 외인 일별 보유량 추이 -> opt10008 "변동수량"
    # 기관 일별 보유량 추이 -> opt10009 "변동수량"
    # 투데이 
# 1. 외국인 상위 불러오기 '코스닥' 1분에 1번씩 -> opt10034
# 2. 항목의 금일 기관 매수세 확인 - 1분에 한번씩 
# 1,2 : 누적으로 신규로 생성된것은 계속 추가 하는 방식 
# 2. 기업 정보 확인 
    # PER = 0 ~ 50
    # PBR = 0.7 ~ 2.5
    # ROE > 0 
    # EPS > 0  
# 3. 상위항목이 기관 매도세면 Remove