import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import datetime 
from QtUiHelper import *
import inspect

class LogViewer(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        # set Dialog Instance
        self.dialog = QDialog()
        self.dialog.setGeometry(0,250,350, 200)

        # set TestEdit 
        self.dialog.setWindowTitle("Log Viewer")
        self.textEdit = QTextEdit(self.dialog)
        self.textEdit.setEnabled(True)
        self.textEdit.verticalScrollBar().setValue(self.textEdit.verticalScrollBar().minimum())

        #set button
        # self.searchBtn = UiHelper.createButton(self, "Search", self.searchHandler)
        # self.searchWithTradeBtn = UiHelper.createButton(self, "Trade", self.searchTradeHandler)

        # set Layout
        self.mainLayout = QVBoxLayout()
        # HlayOut = QHBoxLayout()
        layOut = QVBoxLayout()
        # HlayOut.addWidget(self.searchBtn)
        # HlayOut.addWidget(self.searchWithTradeBtn)
        layOut.addWidget(self.textEdit)
        # self.mainLayout.addLayout(HlayOut)
        self.mainLayout.addLayout(layOut)
        self.dialog.setLayout(self.mainLayout)
    
    def log(self, log):
        if len(log) <= 0: return
        currentTime = datetime.datetime.now()
        timeStr = str(currentTime).split(".")[0]
        self.textEdit.append(timeStr + " : " + log)
    
    def showLogDialog(self):
        print("stop logger dialog")
        self.dialog.show()
    
    def stopLogger(self):
        print("stop logger dialog")
        self.done()

    